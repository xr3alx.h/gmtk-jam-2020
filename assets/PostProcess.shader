shader_type canvas_item;
render_mode unshaded;

uniform sampler2D transition_texture;
uniform float transition_cutoff;

void fragment() {
	vec4 transition_color = texture(transition_texture, vec2(SCREEN_UV.x, 1.0 - SCREEN_UV.y));
	transition_color.r = 1.0 - transition_color.r;
	
	COLOR = mix(vec4(0,0,0,1), vec4(0), smoothstep(transition_color.r, transition_color.r + 0.01, transition_cutoff));
}