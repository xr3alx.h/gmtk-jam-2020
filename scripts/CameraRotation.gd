extends Spatial
class_name CameraRotation

onready var _camera: Camera = $RotationHelper/Camera
onready var _rotation_helper: Spatial = $RotationHelper

var MOUSE_SENSITIVITY: float = 0.05

func _ready() -> void:
	pass
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		_rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
		
		var camera_rot = _rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		_rotation_helper.rotation_degrees = camera_rot;
