extends CanvasLayer

signal transition_finished

var fade_texture_rect: ColorRect
var fade_shader_material: ShaderMaterial
var transition_tween: Tween

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS
	layer = 2
	_create_fade_post_process(true)

func _create_fade_post_process(start_with_black_fade: bool):
	fade_texture_rect = ColorRect.new()
	fade_texture_rect.rect_size = Vector2(1920, 1080)
	fade_texture_rect.mouse_filter = Control.MOUSE_FILTER_IGNORE
	fade_texture_rect.visible = start_with_black_fade

	fade_shader_material = ShaderMaterial.new()
	fade_shader_material.shader = load("res://assets/PostProcess.shader")
	fade_shader_material.set_shader_param("transition_texture", load("res://assets/transition.svg"))
	fade_texture_rect.material = fade_shader_material

	transition_tween = Tween.new()
	transition_tween.connect("tween_all_completed", self, "_on_transition_tween_completed")
	add_child(transition_tween)
	add_child(fade_texture_rect)

func to_black(speed_multiplier: float = 1.0):
	set_visibility(true)
	transition_tween.remove_all()
	transition_tween.interpolate_method(self, "animate_transition", 1.1, -0.1, 1.5 * speed_multiplier, Tween.TRANS_EXPO, Tween.EASE_IN)
	transition_tween.start()

func from_black(speed_multiplier: float = 1.0):	
	transition_tween.remove_all()
	transition_tween.interpolate_method(self, "animate_transition", -0.1, 1.1, 1.5 * speed_multiplier, Tween.TRANS_EXPO, Tween.EASE_IN)
	transition_tween.start()
	yield(transition_tween, "tween_all_completed")
	set_visibility(false)

func animate_transition(value: float):
	fade_shader_material.set_shader_param("transition_cutoff", value)

func _on_transition_tween_completed():
	emit_signal("transition_finished")

func set_visibility(b: bool):
	fade_texture_rect.visible = b
