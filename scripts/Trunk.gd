extends Spatial

signal stuff_dropped(side_offset, type_id)

var STUFF_BOX_SCENE = preload("res://objects/StuffBox.tscn")

onready var _stuff_container: Spatial = $StuffContainer
onready var _tween: Tween = $Tween

var _type_id: int
var _side_offset: float

func _ready() -> void:
	_spawn_stuff()

func _spawn_stuff():
	_type_id = clamp(int(rand_range(1, 7)), 1, 6)
	if _type_id != 6:
		_side_offset = rand_range(-0.5, 0.5)
	else:
		_side_offset = 0
	
	var stuff = STUFF_BOX_SCENE.instance()
	stuff.set_type(_type_id)
	stuff.move = false
	stuff.is_in_main_world(false)
	stuff.transform.origin.y = 0.25
	stuff.transform.origin.x = _side_offset
	_stuff_container.add_child(stuff)
	
	var target: Transform = stuff.transform
	target.origin.z = -3.1
	
	var delay = rand_range(1.5,4)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(stuff, "transform", stuff.transform, target, delay, Tween.TRANS_CUBIC, Tween.EASE_IN)
	
	var target2 = target
	target2.origin.y = -1
	target2.origin.z = -4.5
# warning-ignore:return_value_discarded
	_tween.interpolate_property(stuff, "transform", target, target2, 1, Tween.TRANS_CUBIC, Tween.EASE_IN, delay)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(stuff, "rotation_degrees", Vector3.ZERO, Vector3(-90, 0, 0), 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN, delay)
# warning-ignore:return_value_discarded
	_tween.start()

func _on_Tween_tween_all_completed() -> void:
	emit_signal("stuff_dropped", _side_offset, _type_id)
	_spawn_stuff()
