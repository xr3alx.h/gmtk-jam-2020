extends Spatial

onready var _tween: Tween = $Tween
onready var _camera_rotation: CameraRotation = $CameraRotation

var _current_side_left: bool = false
onready var _side: float = transform.origin.z
var _wiggle_time: float = 0.0

func _physics_process(delta: float) -> void:
	if !_tween.is_active():
		transform.origin.z = _side + (0.2 * sin(_wiggle_time))
		_wiggle_time += delta

func _on_Area_area_entered(area: Area) -> void:
	var target: Transform = transform
	target.origin.z = -1.526 if !_current_side_left else 1.526
	_side = target.origin.z
	_wiggle_time = 0
	
# warning-ignore:return_value_discarded
	_tween.interpolate_property(self, "transform", transform, target, 1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
	var target_rot = _camera_rotation.rotation
	target_rot.y = deg2rad(-60) if _current_side_left else deg2rad(-120)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_camera_rotation, "rotation", _camera_rotation.rotation, target_rot, 1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()
	_current_side_left = !_current_side_left
