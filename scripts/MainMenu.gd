extends Control

onready var _tween: Tween = $Tween
onready var _play_btn: Button = $PlayBtn
onready var _how_to_play_btn: Button = $HowToPlayBtn
onready var _exit_btn: Button = $ExitBtn

func _on_PlayBtn_pressed() -> void:
	$"../Click".play()
	PostProcess.to_black(0.5)
	yield(PostProcess, "transition_finished")
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Game.tscn")

func _on_HowToPlayBtn_pressed() -> void:
	$"../Click".play()
	$"../AnimationPlayer".play("to_howtoplay")

func _on_ExitBtn_pressed() -> void:
	$"../Click".play()
	PostProcess.to_black(0.5)
	yield(PostProcess, "transition_finished")
	get_tree().quit()

func _on_PlayBtn_mouse_entered() -> void:
	$"../Hover".play()
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_play_btn, "rect_scale", _play_btn.rect_scale, Vector2.ONE, 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_exit_btn, "rect_scale", _exit_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_how_to_play_btn, "rect_scale", _how_to_play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()

func _on_ExitBtn_mouse_entered() -> void:
	$"../Hover".play()
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_exit_btn, "rect_scale", _exit_btn.rect_scale, Vector2.ONE, 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_how_to_play_btn, "rect_scale", _how_to_play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_play_btn, "rect_scale", _play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()

func _on_Btn_mouse_exited() -> void:
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_exit_btn, "rect_scale", _exit_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_how_to_play_btn, "rect_scale", _how_to_play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_play_btn, "rect_scale", _play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()

func _on_HowToPlayBtn_mouse_entered() -> void:
	$"../Hover".play()
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_exit_btn, "rect_scale", _exit_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_how_to_play_btn, "rect_scale", _how_to_play_btn.rect_scale, Vector2.ONE, 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.interpolate_property(_play_btn, "rect_scale", _play_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
# warning-ignore:return_value_discarded
	_tween.start()
