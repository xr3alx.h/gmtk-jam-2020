extends Spatial

var RANDOM_CAR_SCENE = preload("res://objects/RandomCar.tscn")
var STUFF_BOX_SCENE = preload("res://objects/StuffBox.tscn")
var VFX_STUFF_BOX_IMPACT = preload("res://objects/VFX_StuffBox_Impact.tscn")

onready var _left_mirror_view: ViewMirror = $LeftMirrorViewport/ViewLeftMirror
onready var _right_mirror_view: ViewMirror = $RightMirrorViewport/ViewRightMirror
onready var _car: Spatial = $Car
onready var _truck: Spatial = $Truck
onready var _level: Level = $Level
onready var _car_spawn_left_lane: Position3D = $Positions/CarSpawnLeftLane
onready var _car_spawn_right_lane: Position3D = $Positions/CarSpawnRightLane
onready var _stuff_drop_position: Position3D = $Truck/StuffDropPosition
onready var _map_view: MapView = $MapViewport/MapView

var _car_spawn_timer: float
var _stuff_drop_timer: float 

var damage_points: int = 0

func _ready() -> void:
	Globals.move_world = true
	_car_spawn_timer = rand_range(5, 10)
	_stuff_drop_timer = rand_range(3, 6)
	PostProcess.from_black(0.5)

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("exit_game"):
		PostProcess.to_black(0.5)
		yield(PostProcess, "transition_finished")
		get_tree().change_scene("res://scenes/Menu.tscn")

func _physics_process(delta: float) -> void:
	_map_view.fill_line.fill_percentage += delta * 0.01
	
	if _map_view.fill_line.fill_percentage >= 1.0:
		set_physics_process(false)
		Globals.move_world = false
		PostProcess.to_black(0.5)
		yield(PostProcess, "transition_finished")
		get_tree().change_scene("res://scenes/Win.tscn")
	
	_car_spawn_timer -= delta
	if _car_spawn_timer <= 0:
		_car_spawn_timer = rand_range(1.5, 4)
		var spawn_side_left: bool = true if randf() > 0.5 else false
		var spawn_position: Vector3 = _car_spawn_left_lane.transform.origin if spawn_side_left else _car_spawn_right_lane.transform.origin
		
		var type_id = clamp(int(rand_range(1, 6)), 1, 5)
		
		var car2 = RANDOM_CAR_SCENE.instance()
		car2.set_type(type_id)
		car2.is_in_main_world(false)
		car2.transform.origin = spawn_position + Vector3(0, 0.924, 0)
		_left_mirror_view.level.add_child(car2)
		
		var car3 = RANDOM_CAR_SCENE.instance()
		car3.set_type(type_id)
		car3.is_in_main_world(false)
		car3.transform.origin = spawn_position + Vector3(0, 0.924, 0)
		_right_mirror_view.level.add_child(car3)
		
		var car = RANDOM_CAR_SCENE.instance()
		car.set_type(type_id)
		car.transform.origin = spawn_position + Vector3(0, 0.924, 0)
		car.car2 = car2
		car.car3 = car3
		_level.add_child(car)
	
#	_stuff_drop_timer -= delta
#	if _stuff_drop_timer <= 0:
#		_stuff_drop_timer = rand_range(1, 6)
#
#		var box = STUFF_BOX_SCENE.instance()
#		box.transform.origin = _stuff_drop_position.global_transform.origin
#		_level.add_child(box)
#
#		box = STUFF_BOX_SCENE.instance()
#		box.is_in_main_world(false)
#		box.transform.origin = _stuff_drop_position.global_transform.origin
#		_left_mirror_view.level.add_child(box)
#
#		box = STUFF_BOX_SCENE.instance()
#		box.is_in_main_world(false)
#		box.transform.origin = _stuff_drop_position.global_transform.origin
#		_right_mirror_view.level.add_child(box)
	
	_left_mirror_view.car.transform.origin = _car.transform.origin
	_right_mirror_view.car.transform.origin = _car.transform.origin
	_left_mirror_view.truck.transform.origin = _truck.transform.origin
	_right_mirror_view.truck.transform.origin = _truck.transform.origin

# warning-ignore:unused_argument
func _on_Area_area_entered(area: Area) -> void:
	if area.is_in_group("Stuff"):
		area.monitorable = false
		var box = area.get_parent().get_parent()

		var impact = VFX_STUFF_BOX_IMPACT.instance()
		impact.transform.origin = box.box2.transform.origin
		box.box2.get_parent().add_child(impact)

		impact = VFX_STUFF_BOX_IMPACT.instance()
		impact.transform.origin = box.box3.transform.origin
		box.box3.get_parent().add_child(impact)

		box.box2.queue_free()
		box.box3.queue_free()
		box.queue_free()

		damage_points += 1
		$UI/Control/DamagePoints.text = "Damage: " + str(damage_points) + "/3"
		$Car/ImpactBox.play()

		if damage_points >= 3:
			Globals.move_world = false
			PostProcess.to_black(0.5)
			yield(PostProcess, "transition_finished")
			get_tree().change_scene("res://scenes/FailedByStuff.tscn")

	elif area.is_in_group("RandomCar"):
		$Car/ImpactCar.play()
		Globals.move_world = false
		PostProcess.to_black(0.5)
		yield(PostProcess, "transition_finished")
		get_tree().change_scene("res://scenes/FailedByCar.tscn")

func _on_Trunk_stuff_dropped(side_offset, type_id) -> void:
	var position = _stuff_drop_position.global_transform.origin
	position.z += side_offset
	
	var box = STUFF_BOX_SCENE.instance()
	box.set_type(type_id)
	box.transform.origin = _stuff_drop_position.global_transform.origin
	box.rotation_degrees.y = 90
	_level.add_child(box)
	
	var box2 = STUFF_BOX_SCENE.instance()
	box2.set_type(type_id)
	box2.is_in_main_world(false)
	box2.transform.origin = _stuff_drop_position.global_transform.origin
	box2.rotation_degrees.y = 90
	_left_mirror_view.level.add_child(box2)
	
	var box3 = STUFF_BOX_SCENE.instance()
	box3.set_type(type_id)
	box3.is_in_main_world(false)
	box3.transform.origin = _stuff_drop_position.global_transform.origin
	box3.rotation_degrees.y = 90
	_right_mirror_view.level.add_child(box3)
	
	box.box2 = box2
	box.box3 = box3
