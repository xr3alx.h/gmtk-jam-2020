extends Line2D
class_name MapFillLine

export(float) var fill_percentage = 0.0 setget set_fill_percentage,get_fill_percentage

onready var _parent_line: Line2D = get_parent()
var _full_length: float = 0.0

func _ready() -> void:
	_full_length = _get_parent_length()

func set_fill_percentage(val: float):
	fill_percentage = val
	clear_points()
	
	var fill_length: float = _full_length * fill_percentage
	var length: float = 0.0
	
	if fill_percentage > 0 && fill_percentage < 1.0:
		add_point(_parent_line.points[0])
		var last_point: Vector2 = _parent_line.points[0]
		for i in range(1, _parent_line.get_point_count()):
			var point: Vector2 = _parent_line.points[i]
			
			var segment_length: float = point.distance_to(last_point)
			if length + segment_length > fill_length:
				var segment_fill_percentage: float = (fill_length - length) / segment_length
				var segment_point = last_point + segment_fill_percentage * (point - last_point)
				add_point(segment_point)
				break
				
			else:
				length += segment_length
				last_point = point
				add_point(point)
		
	elif fill_percentage >= 1.0:
		points = _parent_line.points

func _get_parent_length() -> float:
	var length: float = 0.0
	
	var last_point: Vector2 = _parent_line.points[0]
	for i in range(1, _parent_line.get_point_count()):
		var point: Vector2 = _parent_line.points[i]
		length += point.distance_to(last_point)
		last_point = point
	
	return length

func get_fill_percentage() -> float:
	return fill_percentage
