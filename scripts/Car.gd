extends Spatial

const CAR_SPEED: float = 4.0

var _steer_force: float = 0.5

func _physics_process(delta: float) -> void:
	if Input.is_action_pressed("car_left"):
		transform.origin.z -= CAR_SPEED * delta * _steer_force
		_steer_force = clamp(_steer_force+delta, 0.5, 1.0)
		
	elif Input.is_action_pressed("car_right"):
		transform.origin.z += CAR_SPEED * delta * _steer_force
		_steer_force = clamp(_steer_force+delta, 0.5, 1.0)
		
	else:
		_steer_force = clamp(_steer_force - delta * 0.5, 0.5, 1.0)
	
	if transform.origin.z > 3.5 || transform.origin.z < -3.5:
		get_tree().paused = true
		$"../BackgroundMusic".stop()
		PostProcess.to_black(0.5)
		yield(PostProcess, "transition_finished")
		get_tree().paused = false
		get_tree().change_scene("res://scenes/FailedByOffroad.tscn")
