extends Spatial
class_name Level

export(bool) var is_in_main_world: bool = true

func _ready() -> void:
	_set_main_world_children(self)

func _set_main_world_children(node):
	for child in node.get_children():
		if child is ScrollingObject:
			child.is_in_main_world = is_in_main_world
		
		if child.get_child_count() > 0:
			_set_main_world_children(child)
