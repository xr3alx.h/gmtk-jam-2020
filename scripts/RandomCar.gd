extends Spatial
class_name RandomCar

const SCROLL_SPEED: float = 13.0

var _init_scale: Vector3 = scale
var _init_z_pos: float
var _is_main: bool = true
var _type_id: int

var car2
var car3

func _ready() -> void:
	_init_z_pos = transform.origin.z + rand_range(-0.5, 0.5)
	_init_scale = scale
	scale = Vector3.ZERO

func set_type(type_id):
	_type_id = type_id
	for i in range(1, 6):
		if type_id == i:
			get_node("Type" + str(i)).visible = true
		else:
			get_node("Type" + str(i)).queue_free()

func _physics_process(delta: float) -> void:
	if Globals.move_world:
		if transform.origin.x <= -75:
			set_physics_process(false)
			queue_free()
		
		if transform.origin.x < -70 || transform.origin.x > 70:
			var sc = (75.0 - abs(transform.origin.x)) / 5.0
			scale = _init_scale * sc
		
		if _is_main && car2 != null && car3 != null:
			transform.origin.x -= SCROLL_SPEED * delta
			transform.origin.z = _init_z_pos + 0.2 * sin(transform.origin.x * 0.4)
			
			car2.transform.origin = transform.origin
			car3.transform.origin = transform.origin
		

func is_in_main_world(val: bool):
	_is_main = val
	get_node("Type" + str(_type_id) + "/Area").monitorable = val
