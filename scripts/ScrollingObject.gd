extends MeshInstance
class_name ScrollingObject

const SCROLL_SPEED: float = 15.0

onready var _init_scale: Vector3 = scale
var is_in_main_world: bool = true

func _physics_process(delta: float) -> void:
	if Globals.move_world:
		visible = (transform.origin.x > 4) if is_in_main_world else (transform.origin.x < 16)
		
		transform.origin.x -= SCROLL_SPEED * delta
		if transform.origin.x < -75:
			transform.origin.x = 75
		
		if transform.origin.x < -70 || transform.origin.x > 70:
			var sc = (75.0 - abs(transform.origin.x)) / 5.0
			scale = _init_scale * sc
