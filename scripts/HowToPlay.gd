extends Control

onready var _tween: Tween = $Tween
onready var _got_it_btn: Button = $GotItBtn

func _on_GotItBtn_pressed() -> void:
	$"../Click".play()
	$"../AnimationPlayer".play_backwards("to_howtoplay")

func _on_GotItBtn_mouse_entered() -> void:
	$"../Hover".play()
	# warning-ignore:return_value_discarded
	_tween.interpolate_property(_got_it_btn, "rect_scale", _got_it_btn.rect_scale, Vector2.ONE, 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
	# warning-ignore:return_value_discarded
	_tween.start()

func _on_GotItBtn_mouse_exited() -> void:
	# warning-ignore:return_value_discarded
	_tween.interpolate_property(_got_it_btn, "rect_scale", _got_it_btn.rect_scale, Vector2(0.9,0.9), 0.1, Tween.TRANS_QUINT, Tween.EASE_IN_OUT)
	# warning-ignore:return_value_discarded
	_tween.start()
